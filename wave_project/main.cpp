#include <iostream>
#include <cmath>
#include "Darrays.h"
#include <chrono>

using namespace std;

double init_u(double x,double y)
{
    double sig = 0.1;
    return exp(-0.5*(x*x+y*y)/(sig*sig));
}

double init_dudt(double x,double y)
{
    return 0.0;
}

int
main(int argc, char** argv){

    auto begin = std::chrono::high_resolution_clock::now();
    
        // Domain size 
    double x_min,x_max,y_min,y_max,h,dt,T_final,t;
    int nt;
    int N = 20;
    int M = 20;
    int output_viz = 0;
    
    // Accept input numbers for number of discretization points M & N
    // 
    if (argc > 1)
        M = atoi(argv[1]);

    if (argc > 2)
        N = atoi(argv[2]);
        // Output every output_viz soluiton file
    if (argc > 3)
        output_viz = atoi(argv[3]);

    T_final = 10.0;
    x_min = -1.0;
    x_max = 1.0;
        // Space step
    h = (x_max-x_min)/(M-1);
    double h2i=1.0/h/h;

    y_min = -1.0;
    y_max = y_min + (N-1)*h;

        // timestep;
    dt = 0.5*h/sqrt(2.0);
    cout << "Timestep size: " << dt << endl;
    nt = ceil(T_final/dt) ;
    dt = T_final/nt;
    double dt2=dt*dt;
    cout << "Number of timesteps: " << nt << endl;
    
    Darray1 x,y;
    x.define(1,M);
    y.define(1,N);

        // set up grid
    for (int i = 1; i <=M ; i++)
        x(i) = x_min + h*(i-1);

    for (int j = 1; j <=N ; j++)
        y(j) = y_min + h*(j-1);
    
    Darray2 u,up,um,Lap;
    u.define(1,M,1,N);
    up.define(1,M,1,N);
    um.define(1,M,1,N);
    Lap.define(1,M,1,N);
    for (int j = 1; j <=N ; j++)
        for (int i = 1; i <=M ; i++){
                // Initial data and initialization
            um(i,j) = init_u(x(i),y(j)) - dt*init_dudt(x(i),y(j));
            u(i,j) = init_u(x(i),y(j));
            up(i,j) = 0.0;
            Lap(i,j) = 0.0;
        }
        // Put viz. output at timestep 0 here.

    char fName[100];
    int ifile = 0;
    snprintf(fName, 100, "u_sol_%06d.txt",ifile);
    u.writeToFile(fName,1,M,1,N);
    
    snprintf(fName, 100, "x.txt");
    x.writeToFile(fName,1,M);
    snprintf(fName, 100, "y.txt");
    y.writeToFile(fName,1,N);
    
        // Do some timestepping
    for (int it = 1; it <=nt; it++){
            // current time
        t = (it-1)*dt;
            // Compute Laplacian in interior points
        for (int j = 2; j <= N-1 ; j++)
            for (int i = 2; i <= M-1 ; i++){
                Lap(i,j) = h2i*(-4*u(i,j)
                                +u(i-1,j)
                                +u(i+1,j)
                                +u(i,j-1)
                                +u(i,j+1));
            }

            // Update time levels 
        for (int j = 2; j <= N-1 ; j++)
            for (int i = 2; i <= M-1 ; i++){
                up(i,j) = 2.0*u(i,j)-um(i,j) + dt2*Lap(i,j);
                um(i,j) = u(i,j);
                u(i,j) = up(i,j);
            }
            // Impose boundary conditions

        for (int j = 1; j <=N ; j++)
        {
            u(1,j) = 0.0;
            u(N,j) = 0.0;
        }
        
        
        for (int i = 1; i <=M ; i++){
            u(i,1) = 0.0;
            u(i,M) = 0.0;
        }
            // Put viz. output here.

        if (output_viz > 0){
            if (it%output_viz == 0){
                ifile++;
                snprintf(fName, 100, "u_sol_%06d.txt",ifile);
                u.writeToFile(fName,1,M,1,N);
            }
        }
        
    } // End time loop

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    cout << "Time to compute [s]: " << elapsed.count()*1e-9 << endl;
    
    return 0;
}
