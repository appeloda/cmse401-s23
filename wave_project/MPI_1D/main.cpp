#include <iostream>
#include <cmath>
#include "Darrays.h"
#include <chrono>
#include <stdlib.h>

/* Get the MPI header file */
#include <mpi.h>

using namespace std;

double init_u(double x)
{
    double sig = 0.1;
    return exp(-0.5*(x*x)/(sig*sig));
}

double init_dudt(double x)
{
    return 0.0;
}

int
main(int argc, char** argv){


        /* Declare variables */
    int   rank, size;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

        // Domain size 
    double x_min,x_max,h,dt,T_final,t;
    int nt;
    int M = 10;
    int output_viz = 0;
    // Accept input numbers for number of discretization points M 
    // 
    if (argc > 1)
        M = atoi(argv[1]);

    if (argc > 2)
        output_viz = atoi(argv[2]);
    
    int M_l = M/size;
    int rem = M%size;
    
/**
   if (rank==0){
   cout << "Each proc will have at least " << M_l<< " elements." << endl;
   cout << "Some " << rem<< " will have one additional element." << endl;
   }
**/
   
    if (rank < rem)
     M_l++;
   
    int istart;
    int iend;
    if (rank<rem)
        istart = 1 + rank*M_l;

    if(rank>=rem)
        istart = 1 + rem*(M_l+1)+(rank-rem)*M_l;

    if (rank<rem)
       iend = (rank+1)*M_l;

    if(rank>=rem)
        iend = rem*(M_l+1)+(rank-rem+1)*M_l;

    int to_my_left = rank-1;
    int to_my_right = rank+1;
    if (rank == 0)
        to_my_left = MPI_PROC_NULL;
    if (rank == size-1)
        to_my_right = MPI_PROC_NULL;
    
//    cout << "I am proc " << rank << " and I will start my index at " << istart << endl;
//    cout << "I am proc " << rank << " and I will end my index at " << iend << endl;

    T_final = 1.5;
    x_min = -1.0;
    x_max = 1.0;
        // Space step
    h = (x_max-x_min)/(M-1);
    double h2i=1.0/h/h;
    Darray1 x;
    // The global array should have M elements from 1 to M
    // But we only want to store M_l elements on each process.
    // THIS ARRAY NOW IS SMALL AND ONLY DEFINED ON processor "rank".
    x.define(istart,iend);
        // set up grid
    for (int i = istart; i <=iend ; i++)
        x(i) = x_min + h*(i-1);

    Darray1 u; 
    u.define(istart-1,iend+1); // This includes a "halo region" aka ghost-points

        // timestep;
    dt = 0.9*h;

    nt = ceil(T_final/dt) ;
    dt = T_final/nt;
    double dt2=dt*dt;
    if (rank == 0){
        cout << "Number of timesteps: " << nt << endl;
        cout << "Timestep size: " << dt << endl;
    }
    
    Darray1 up,um,Lap;
    up.define(istart,iend);
    um.define(istart,iend);
    Lap.define(istart,iend);
    
    for (int i = istart; i <=iend ; i++){
        um(i) = init_u(x(i)) - dt*init_dudt(x(i));
        u(i) = init_u(x(i));
    }
    
        // Put viz. output at timestep 0 here.

    char fName[100];
    int ifile = 0;
    if (output_viz > 0){
        snprintf(fName, 100, "u_sol_%06d_%06d.txt",ifile,rank);
        u.writeToFile(fName,istart,iend);
        snprintf(fName, 100, "x_%06d.txt",rank);   
        x.writeToFile(fName,istart,iend);
    }
    
    double usl,usr,url,urr;

        // Do some timestepping, i.e. solve the wave equation
    for (int it = 1; it <=nt; it++){
            // current time
        t = (it-1)*dt;

            // Set Boundary zero Neumann bondary conditions and 
            // communicate the current u between processes 
            // First BC
        if (rank == 0)
            u(istart-1) = u(istart+1);
        if (rank == size-1)
            u(iend+1) = u(iend-1);

        usl = u(istart);

        MPI_Sendrecv(&usl,1,MPI_DOUBLE,to_my_left,0,
                     &urr,1,MPI_DOUBLE,to_my_right,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
        u(iend+1) = urr;
               
        usr = u(iend);        
        MPI_Sendrecv(&usr,1,MPI_DOUBLE,to_my_right,1,
                     &url,1,MPI_DOUBLE,to_my_left,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
        u(istart-1) = url;
        MPI_Barrier(MPI_COMM_WORLD);
        
                // Compute Laplacian in interior points
        for (int i = istart; i <= iend ; i++){
            Lap(i) = h2i*(-2*u(i)+u(i-1)+u(i+1));
        }
            // Update time levels 
        for (int i = istart; i <= iend ; i++){
            up(i) = 2.0*u(i)-um(i) + dt2*Lap(i);
            um(i) = u(i);
            u(i) = up(i);
        }

        if (output_viz > 0){
            if (it%output_viz == 0){
                ifile++;
                snprintf(fName, 100, "u_sol_%06d_%06d.txt",ifile,rank);
                u.writeToFile(fName,istart,iend);
            }
        }
        
    } // End time loop
    if (rank == 0)
        cout << "Number of files saved: " << ifile+1 << endl;

    MPI_Finalize();
    return 0;
}
