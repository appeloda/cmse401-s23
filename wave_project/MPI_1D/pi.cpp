#include <iostream>
#include <cmath>
#include "Darrays.h"
#include <chrono>
#include <stdlib.h>

/* Get the MPI header file */
#include <mpi.h>

using namespace std;

double integrand(double x)
{
    return 4.0/(1.0+(x*x));
}

int
main(int argc, char** argv){


        /* Declare variables */
    int   proc, rank, size, namelen;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank==0)
        cout << "Number of procs: " << size << endl;
    
    auto begin = std::chrono::high_resolution_clock::now();
    
        // Domain size 
    double x_min,x_max,h;
    int M = 10;
    if (argc > 1)
        M = atoi(argv[1]);

    int M_l = M/size;
    int rem = M%size;

    if (rank==0){
  //      cout << "Each proc will have at least " << M_l<< " elements." << endl;
 //       cout << "Some " << rem<< " will have one additional element." << endl;
    }

    if (rank < rem)
     M_l++;
   
//     cout << "I am proc " << rank << " and I will have " << M_l << " elements." << endl;
    int istart;
    int iend;

    if (rank<rem)
        istart = 1 + rank*M_l;

    if(rank>=rem)
        istart = 1 + rem*(M_l+1)+(rank-rem)*M_l;

    if (rank<rem)
       iend = (rank+1)*M_l;

    if(rank>=rem)
        iend = rem*(M_l+1)+(rank-rem+1)*M_l;

    //cout << "I am proc " << rank << " and I will start my index at " << istart << endl;
    //cout << "I am proc " << rank << " and I will end my index at " << iend << endl;

    x_min = 0.0;
    x_max = 1.0;
        // Space step
    h = (x_max-x_min)/(M-1);

    Darray1 x;
    // The global array should have M elements from 1 to M
    // But we only want to store M_l elements on each process.
    // THIS ARRAY NOW IS SMALL AND ONLY DEFINED ON processor "rank".
    x.define(istart,iend);
        // set up grid
    for (int i = istart; i <=iend ; i++)
        x(i) = x_min + h*(i-1);

    double rsum = 0.0;

    for (int i = istart; i <=iend ; i++)
        rsum = rsum + h*integrand(x(i));

   // cout << "I am proc " << rank << " and my sum is " << rsum << " using "<< M <<" gridpoints" <<endl;

    double pi_tmp;
    int one = 1;
    int master = 0;
    if (rank == master){
        for (int proc = 1; proc < size ; proc++){
            MPI_Recv(&pi_tmp,one,MPI_DOUBLE,proc,proc,MPI_COMM_WORLD,&status);
            rsum = rsum + pi_tmp;
        }
    }
    else
    {
        MPI_Send(&rsum,1,MPI_DOUBLE,master,rank,MPI_COMM_WORLD);
    }
    
    if (rank == 0)
        cout << "I am proc " << rank << " and my sum is " << rsum << " using "<< M <<" gridpoints" <<endl;
    
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
//    cout << "Time to compute [s]: " << elapsed.count()*1e-9 << endl;
    MPI_Finalize();
    return 0;
}
