#include <iostream>
#include <cmath>
#include "Darrays.h"
#include <chrono>
#include <stdlib.h>

/* Get the MPI header file */
#include <mpi.h>

using namespace std;


int
main(int argc, char** argv){

        // This code sends a double array of size data_size
        // to the next rank if rank < size-1, if rank = size-1 then send to process = 0     
        //
    
        /* Declare variables */
    int rank, size;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank==0)
        cout << "Number of procs: " << size << endl;
    
    int M = 1;
    int data_size = 1;
    // Case
    if (argc > 1)
        M = atoi(argv[1]);
    if (argc > 2)
        data_size = atoi(argv[2]);

    double u_send[data_size];
    double u_recv[data_size];    

        // Blocking send and recv

    int rank_to;
    int rank_from;

    rank_to = (rank == size-1) ? 0 : rank+1;
    rank_from = (rank == 0) ? size-1 : rank-1;

    for (int i = 0; i < data_size; i++)
        u_send[i] = 1.0*rank;

    if (M==1){
            // For how large data_size does this work?
        MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,MPI_COMM_WORLD);
        MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,0,MPI_COMM_WORLD,&status);
    }
    else if (M==2){
            // Why does this work?
        if (rank%2 == 0){
            MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,MPI_COMM_WORLD);
            MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,0,MPI_COMM_WORLD,&status);
        }
        else{
            MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,0,MPI_COMM_WORLD,&status);
            MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,MPI_COMM_WORLD);
        }
    }
    else if (M==3){
        MPI_Request request;
            // MPI_Request represents a handle on a non-blocking operation.
            // This is used by wait (MPI_Wait, MPI_Waitall, MPI_Waitany, MPI_Waitsome)
            // and test (MPI_Test, MPI_Testall, MPI_Testany, MPI_Testsome) to know
            // when the non-blocking operation handled completes.
            // We don't use it here.
        MPI_Isend(u_send,data_size,MPI_DOUBLE,rank_to,0,MPI_COMM_WORLD,&request);
        MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,0,MPI_COMM_WORLD,&status);
    }
    else if (M==4){
            // Safest and easiest if you have a simple communication pattern
        MPI_Sendrecv(u_send,data_size,MPI_DOUBLE,rank_to,0,
                     u_recv,data_size,MPI_DOUBLE,rank_from,0,MPI_COMM_WORLD,&status);
    }
    
    MPI_Finalize();
    return 0;
}
