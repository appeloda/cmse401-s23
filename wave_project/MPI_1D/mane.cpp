#include <iostream>
#include <stdlib.h>

/* Get the MPI header file */
#include <mpi.h>

using namespace std;

int
main(int argc, char** argv){

        // This code sends a double array of size data_size
        // to the next rank if rank < size-1, if rank = size-1 then send to process = 0     
        //
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

//    cout << "I am : " << rank << " out of " << size << endl;

    int rank_to;
    int rank_from;

    rank_to = (rank == size-1) ? 0 : rank+1;
    rank_from = (rank == 0) ? size-1 : rank-1;

    int data_size = 1;
    int cse = 1;
    
    if(argc > 1)
        data_size = atoi(argv[1]);
    if(argc > 2)
        cse = atoi(argv[2]);
    
    double * u_send;
    double * u_recv;
    
    u_send = new double [data_size];
    u_recv = new double [data_size];
    
    for(int i = 0; i<data_size; i++)
        u_send[i] = rank;
    MPI_Status status;
    MPI_Request request;
    if (cse == 1){
        MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,
                 MPI_COMM_WORLD);
        MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,MPI_ANY_TAG,
                 MPI_COMM_WORLD,&status);
    }
    else if(cse == 2)
    {
       
        MPI_Isend(u_send,data_size,MPI_DOUBLE,rank_to,0,
                  MPI_COMM_WORLD,&request);
        MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,MPI_ANY_TAG,
                 MPI_COMM_WORLD,&status);
        MPI_Wait(&request, &status);
    }
    else if(cse == 3)
    {
        if(rank%2 == 0){
            MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,
                     MPI_COMM_WORLD);
            MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,MPI_ANY_TAG,
                     MPI_COMM_WORLD,&status);
        }
        else
        {
            MPI_Recv(u_recv,data_size,MPI_DOUBLE,rank_from,MPI_ANY_TAG,
                     MPI_COMM_WORLD,&status);
            MPI_Send(u_send,data_size,MPI_DOUBLE,rank_to,0,
                     MPI_COMM_WORLD);
        }
    }
    else if(cse == 4)
    {
            // Safest and easiest
        MPI_Sendrecv(u_send,data_size,MPI_DOUBLE,rank_to,0,
                     u_recv,data_size,MPI_DOUBLE,rank_from,0,
                     MPI_COMM_WORLD,&status);
        
    }
    
    
    cout << "I am : " << rank << " and I got " << u_recv[0] << endl;

    delete[] u_send;
    delete[] u_recv;
    MPI_Finalize();
    return 0;
    
    
}
