#include <iostream>
#include <cmath>
#include "Darrays.h"
#include <chrono>
#include <random>
#include <omp.h>

using namespace std;

int main(int argc, char *argv[]){

    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0,1.0);

    // matrix size
    int N = 10, M = 10, K = 10;

// Accept input numbers for array sizes (m,k,n)
    if (argc > 1)
        M = atoi(argv[1]);

    if (argc > 2)
        K = atoi(argv[2]);

    if (argc > 3)
        N = atoi(argv[3]);

    Darray2 A,B,C; // Define Matrices
    A.define(1,M,1,K);
    B.define(1,K,1,N);
    C.define(1,M,1,N);
    for (int j = 1; j <= K ; j++)
      for (int i = 1; i <= M ; i++)
    	A(i,j) = distribution(generator);

    for (int j = 1; j <= N ; j++)
      for (int i = 1; i <= K ; i++)
    	B(i,j) = distribution(generator);

//  Only time the actual matmul
    auto begin = std::chrono::high_resolution_clock::now();

    for (int j = 1; j <= N ; j++)
      for (int i = 1; i <= M ; i++)
      {
        double cij = 0.0;
         for (int k = 1; k <= K ; k++)
            cij += A(i,k)*B(k,j);
        C(i,j) = cij;
      }    

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    cout << "Matmul 1 [s]: " << elapsed.count()*1e-9 << endl;
   
//  Only time the actual matmul
    begin = std::chrono::high_resolution_clock::now();

    for (int j = 1; j <= N ; j++)
      for (int i = 1; i <= M ; i++)
      {
        double cij = 0.0;
         for (int k = 1; k <= K ; k++)
            cij += A(i,k)*B(k,j);
        C(i,j) = cij;
      }    

    end = std::chrono::high_resolution_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    cout << "Matmul 2 [s]: " << elapsed.count()*1e-9 << endl;

//  Only time the actual matmul
    begin = std::chrono::high_resolution_clock::now();

    for (int j = 1; j <= N ; j++)
      for (int i = 1; i <= M ; i++)
      {
        double cij = 0.0;
         for (int k = 1; k <= K ; k++)
            cij += A(i,k)*B(k,j);
        C(i,j) = cij;
      }    

    end = std::chrono::high_resolution_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    cout << "Matmul 3 [s]: " << elapsed.count()*1e-9 << endl;

    return 0;
}
