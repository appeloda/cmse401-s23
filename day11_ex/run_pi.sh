#!/usr/bin/bash

for N in 100 1000 10000 100000 1000000 10000000 100000000
do
    cp day11.cpp apa.cpp;
    sed -i -e "s/XXXXXX/$N/g" apa.cpp;
    g++ apa.cpp;
    declare -a array=( $( { time for((k=1;k<=100;k+=1)); do ./a.out; done } 2>&1 >/dev/null ))
    tmp=${array[3]};
    minute=${tmp:0:1};
    second=${tmp:2:5};
    echo $minute $second | awk '{print 0.01*60*$1 + 0.01*$2}'
#    echo $tmp;
#    echo $minute;
#    echo $second;
done




